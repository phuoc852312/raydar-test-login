var app = require("express")();
var bodyParser = require("body-parser");
var users = require("./src/user");

var options = {
    host: '127.0.0.1',
    port: 1338
}
console.log('Webserver is running...');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.route('/users')
    .get(users.getAll)
    .post(users.create)
    .put()
    .delete();

app.route('/users/login')
    .get()
    .post(users.login)
    .put()
    .delete();

app.route('/users/:id')
    .get(users.getOne)
    .post()
    .put()
    .delete();

app.listen(options.port, options.host);
console.log('Webserver is listening...');