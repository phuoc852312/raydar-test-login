﻿// src/user.js

//lets require/import the mongodb native drivers.
var mongodb = require('mongodb');

//We need to work with "MongoClient" interface in order to connect to a mongodb server.
var MongoClient = mongodb.MongoClient;

// Connection URL. This is where your mongodb server is running.
var url = 'mongodb://localhost:27017/raydartest';

var users = {
    //public api to get all the user
    getAll: function (req, res) {
        // Use connect method to connect to the Server
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            }
            else {
                //HURRAY!! We are connected. :)
                console.log('Connection established to', url);

                // Get the documents collection
                var collection = db.collection('zips');

                // Insert some users
                collection.find({}).toArray
                (
                    function (err, result) {
                        if (err) {
                            console.log(err);
                            res.json(err);
                        } else if (result.length) {
                            console.log(result);
                            //hide password
                            result.forEach(function (item) {
                                item.password = "";
                            });
                            res.json(result);
                        } else {
                            console.log('No document(s) found with defined "find" criteria!');
                            res.json('No document(s) found with defined "find" criteria!');
                        }
                        //Close connection
                        db.close();
                    }
                );
            }
        });
    },

    getOne: function(req, res) {
        var id = req.params.id;
        // Use connect method to connect to the Server
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            }
            else {
                //HURRAY!! We are connected. :)
                console.log('Connection established to', url);

                // Get the documents collection
                var collection = db.collection('zips');

                var ObjectId = require('mongodb').ObjectID;
                // Insert some users
                //console.log(id);
                collection.find({ _id: new ObjectId(id) }).toArray
                (
                    function (err, result) {
                        if (err) {
                            console.log(err);
                            res.json(err);
                        } else if (result.length) {
                            console.log(result);
                            res.json(result);
                        } else {
                            console.log('No document(s) found with defined "find" criteria!');
                            res.json('No document(s) found with defined "find" criteria!');
                        }
                        //Close connection
                        db.close();
                    }
                );
            }
        });
    },

    create: function (req, res) {
        var newuser = req.body;
        //console.log(newuser);
        //console.log(newuser);
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            }
            else {
                //HURRAY!! We are connected. :)
                console.log('Connection established to', url);

                // Get the documents collection
                var collection = db.collection('zips');
                //check exist email
                //console.log("email 1 " + newuser.email);
                collection.find({ 'email': newuser.email }).toArray
                (
                    function (err, result) {
                        if (err) {
                            console.log(err);
                            res.json(err);
                        } else if (result.length) {
                            console.log("result " + result);
                            res.json("The email is exist!");
                        } else {
                            // Insert some users
                            newuser.user_type = "raydar";
                            collection.insert(newuser, function (err, result) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
                                    res.json({ code: 200, message: "Welcome to Raydar, please log in again to use our services!" });
                                }
                                //Close connection
                                db.close();
                            });
                        }
                    }
                );
            }
        });
    },

    login: function (req, res) {
        var user = req.body;
        console.log(user);
        // Use connect method to connect to the Server
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log('Unable to connect to the mongoDB server. Error:', err);
            }
            else {
                //HURRAY!! We are connected. :)
                console.log('Connection established to', url);

                // Get the documents collection
                var collection = db.collection('zips');

                var ObjectId = require('mongodb').ObjectID;
                // Insert some users
                //console.log(id);
                collection.find({ email: user.email }).toArray
                (
                    function (err, result) {
                        if (err) {
                            console.log(err);
                            res.json(err);
                        } else if (result.length) {
                            if (result[0].password == user.password) {
                                res.json({ code: 200, message: "Login successfully!", info: result[0]});
                            } else {
                                res.json('Wrong password');
                            }
                            //console.log(result);
                        } else {
                            console.log('The user have not registered yet!');
                            res.json('The user have not registered yet!');
                        }
                        //Close connection
                        db.close();
                    }
                );
            }
        });
    }
};

module.exports = users;