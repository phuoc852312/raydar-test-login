This is the souce code for the login/signin test.
1. At current, i haven't finish the feature login/signin with google/facebook yet. :(
2. The user can log in and register in traditional way.
3. The website use AngularJS for the client side and NodeJS for the server side.
4. I didn't design database for this because i used nonSQL (MongoDB). By adding a field named 'user_type' to manage the type of user (raydar/facebook/google). You can see the sample data of the website below:
[
	{
	"_id": "576f4cbf8be7c96c28311b55",
	"fullname": "Phuoc Le 1",
	"email": "phuocle1@gmail.com",
	"password": "",
	"user_type": "raydar"
	},
	{
	"_id": "576f4cbf8be7c96c28311b56",
	"fullname": "Phuoc Le 2",
	"email": "phuocle2@gmail.com",
	"password": "",
	"user_type": "facebook",
	"facebook_id": "abc1",
	"likes": "xyz"
	},
	{
	"_id": "576f4cbf8be7c96c28311b57",
	"fullname": "Phuoc Le 3",
	"email": "phuocle3@gmail.com",
	"password": "",
	"user_type": "google",
	"google_id": "abc2"
	}
]
5. Actually, this is the first time i have done by myself from zero to a small website. Because in current company, i can change the project rapidly based on the customer requirements (outsourcing environment), so i can not learn much from the projects. And i will just participate in some projects and develope some features (including fixing bugs) ... I'm very appreciate your test - RAYDAR TEAM and i'm very happy when have chance to meet you! Thanks