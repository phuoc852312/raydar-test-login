'use strict';
var loginApp = angular.module('loginApp', ['ngRoute']);

loginApp
.config(function ($routeProvider) {
    $routeProvider.
    when('/home', {
        templateUrl: 'login.html',
        controller: 'LoginController'
    }).
    when('/profile', {
        templateUrl: 'profile.html',
        controller: 'ProfileController'
    }).
    otherwise({
        redirectTo: '/home'
    });
});