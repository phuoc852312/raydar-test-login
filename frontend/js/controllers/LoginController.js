'use strict';
var server = {
    host: 'http://127.0.0.1',
    port: 1338
}
loginApp.controller('LoginController', 
		function LoginController($rootScope, $scope, $http) {
		    
		    $scope.validateEmail =  function (email) {
		        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		        return re.test(email);
		    }

		    $scope.register = function () {
                //Check data valid
		        if ($scope.info == undefined || $scope.info.fullname == undefined || $scope.info.fullname == "") {
		            alert("Please input full name!");
		            return;
		        }
		        if ($scope.info.email == undefined || $scope.info.email == "") {
		            if (!$scope.validateEmail($scope.info.email)) {
		                alert("Please input the valid email!");
		                return;
		            }
		        }
		        if ($scope.info.password == undefined || $scope.info.password == "") {
		            alert("Please input password!");
		            return;
		        }
		        if ($scope.info.password.length < 8) {
		            alert("The password must be at least 8 characters!");
		            return;
		        }
		        if ($scope.repeatPassword == undefined || $scope.repeatPassword == "") {
		            alert("Please input repeat password!");
		            return;
		        }
		        if ($scope.info.password != $scope.repeatPassword) {
		            alert("Password does not match the repeat password!");
		            return;
		        }
		        //call create a new user api
		        $http({
		            method: "POST",
		            url: server.host + ":" + server.port + "/users",
		            data: "fullname=" + encodeURIComponent($scope.info.fullname) +
                        "&email=" + encodeURIComponent($scope.info.email) +
                     "&password=" + encodeURIComponent($scope.info.password),
		            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		        }).then(function mySucces(response) {
		            //$scope.myWelcome = response.data;
		            if (response.data.code == 200) {
		                alert(response.data.message);
		            }
		            else {
		                alert(response.data);
		            }
		        }, function myError(response) {
		            //$scope.myWelcome = response.statusText;
		            alert("There have something wrong with your register, please come back after a minute!");
		        });
		        console.log($scope.info);
		    }

		    $scope.login = function () {
		        //Check data valid
		        if ($scope.info == undefined || $scope.info.email == undefined || $scope.info.email == "") {
		            if (!$scope.validateEmail($scope.info.email)) {
		                alert("Please input the valid email!");
		                return;
		            }
		        }
		        if ($scope.info.password == undefined || $scope.info.password == "") {
		            alert("Please input password!");
		            return;
		        }
		        if ($scope.info.password.length < 8) {
		            alert("The password must be at least 8 characters!");
		            return;
		        }
		        //call login api
		        $http({
		            method: "POST",
		            url: server.host + ":" + server.port + "/users/login",
		            data: "email=" + encodeURIComponent($scope.info.email) +
                     "&password=" + encodeURIComponent($scope.info.password),
		            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		        }).then(function mySucces(response) {
		            //$scope.myWelcome = response.data;
		            if (response.data.code == 200) {
		                alert(response.data.message);
		                $rootScope.info = response.data.info;
		                window.location.href = window.location.href.replace("home", "profile");
		            }
		            else {
		                alert(response.data);
		            }   
		        }, function myError(response) {
		            //$scope.myWelcome = response.statusText;
		            alert("There have something wrong with your login, please come back after a minute!");
		        });
		        console.log($scope.info);
		    }
	}
);
